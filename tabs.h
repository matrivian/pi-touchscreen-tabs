#ifndef TABS_H_
#define TABS_H_

#include <QtWidgets>
#include <iostream>

class PiTouchTabs : public QDialog
{
    Q_OBJECT

public:
    explicit PiTouchTabs();
private:
    QTabWidget *tabWidget;
};

class TabOne : public QWidget
{
    Q_OBJECT
public:
      explicit TabOne(QWidget *parent = 0);
private:
      QPushButton *list;
      QPushButton *pwd;
      QPushButton *who;
private slots:
    void list_usr();
    void print_wd();
    void print_who();
};

class TabTwo : public QWidget
{
    Q_OBJECT

public:
    explicit TabTwo(QWidget *parent = 0);
private:
    QLabel *label;
};

#endif /* TABS_H_ */
