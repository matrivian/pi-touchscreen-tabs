#include "tabs.h"

PiTouchTabs::PiTouchTabs()
{
    setWindowTitle("Pi TouchScreen Tabs");
    resize(336, 227);

    tabWidget = new QTabWidget;
    tabWidget->addTab(new TabOne(), "Tab1");
    tabWidget->addTab(new TabTwo(), "Tab2");
    tabWidget->setFixedSize(330, 220);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(tabWidget);
    setLayout(mainLayout);
}

TabOne::TabOne(QWidget *parent) : QWidget(parent)
{
    list = new QPushButton();
    pwd = new QPushButton();
    who = new QPushButton();
    QSize buttonsize(64, 64);
    list->setIcon(QIcon("./list.png"));
    list->setIconSize(buttonsize);
    pwd->setIcon(QIcon("./pwd.png"));
    pwd->setIconSize(buttonsize);
    who->setIcon(QIcon("./who.png"));
    who->setIconSize(buttonsize);

    // connect the buttons with the corresponding processes
    QObject::connect(list, SIGNAL(clicked()), this, SLOT(list_usr()));
    QObject::connect(pwd, SIGNAL(clicked()), this, SLOT(print_wd()));
    QObject::connect(who, SIGNAL(clicked()), this, SLOT(print_who()));

    // Layout
    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(list, 0, 0);
    mainLayout->addWidget(pwd, 0, 1);
    mainLayout->addWidget(who, 0, 2);
    setLayout(mainLayout);
}

void TabOne::list_usr()
{
    QProcess process;
    QString cmd = "ls";
    QStringList args;
    args << "-l" << "/usr/";
    process.start(cmd, args);
    process.waitForFinished();

    QString stdOut = process.readAllStandardOutput();
    QString stdErr = process.readAllStandardError();
    std::cout << stdOut.toStdString() << std::endl;
    std::cout << stdErr.toStdString() << std::endl;
}

void TabOne::print_wd()
{
    QProcess process;
    QString cmd = "pwd ";
    process.start(cmd);
    process.waitForFinished();

    QString stdOut = process.readAllStandardOutput();
    QString stdErr = process.readAllStandardError();
    std::cout << stdOut.toStdString() << std::endl;
    std::cout << stdErr.toStdString() << std::endl;
}

void TabOne::print_who()
{
    QProcess process;
    QString cmd = "who ";
    process.start(cmd);
    process.waitForFinished();

    QString stdOut = process.readAllStandardOutput();
    QString stdErr = process.readAllStandardError();
    std::cout << stdOut.toStdString() << std::endl;
    std::cout << stdErr.toStdString() << std::endl;
}

TabTwo::TabTwo(QWidget *parent) : QWidget(parent)
{
    label = new QLabel("LABEL");
    QFont label_font(label->font());
    label_font.setPointSize(20);
    label->setFont(label_font);
    QGridLayout *layout = new QGridLayout;

    layout->addWidget(label, 0, 0);
    setLayout(layout);
}
