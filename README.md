Pi touchscreen UI--Tabs
============
Author: Matrivian

COMPILE
----------------
qmake -project

qmake

make

RUN
----------------
./pi-touch-tabs -platform xcb

Figures
----------------

![alt text](https://bytebucket.org/matrivian/pi-touchscreen-tabs/raw/76f628063bf5c6206d55224509ba2860ef57b406/tab1.png?token=cf52fca250627e5922b3604b12f8f4f07b659788 "Tab One")

![alt text](https://bytebucket.org/matrivian/pi-touchscreen-tabs/raw/28c3e88a90575bde093ca4868bf19d0090d6bccc/tab2.png?token=cc55fb7617d650a0a70fddad571eeb0b7ae2dca4 "Tab Two")

Pressing List Button shows the corresponding standard output

![alt text](https://bytebucket.org/matrivian/pi-touchscreen-tabs/raw/28c3e88a90575bde093ca4868bf19d0090d6bccc/list-func.png?token=7e27b6631f2f71562d3cb545e79904bb19375784 "Pressing List button")


License
-------
This project is released under [Creative Commons Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/deed.en_US).
