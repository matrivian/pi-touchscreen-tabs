#include <QApplication>
#include "tabs.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    
    // style sheet
    QFile File("pi-touch-tabs.stylesheet");
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());
    app.setStyleSheet(StyleSheet);

    // tabs
    PiTouchTabs pi_touch_tabs;
    pi_touch_tabs.show();
    return app.exec();
}
